package com.enetelsolutions.ljekobiljeqa;

import org.testng.annotations.Test;

import java.util.List;

public class ShoppingPickUpTest extends InitTest {

    List<CartItem> cartItems;
    Product product1;


    @Test(priority = 0, description = "Normal product in cart - via pick Up")
    public void pickUpPersonal() {
        driver.get(BASE_URL + url.p3);
        home.cookieAgree();
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;
        sa.assertEquals(spp.cartCounter(), expected);

        spp.goToCart();
        cartItems = ci.getCartItems();
        sa.assertEquals(cartItems.get(index).productName, product1.productName);
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);

        cart.getBasketValues();
        sa.assertEquals(cart.orderProductsTotal, product1.productPrice);
        sa.assertTrue(cart.orderShippingTotal != 0);
        sa.assertEquals(cart.orderTotalAmount, cart.orderProductsTotal + cart.orderShippingTotal);

        cart.goToCheckoutPage();

        checkout.deliveryMethodSelection(2);
        Customer personal = new Customer(FIRST_NAME_PERSONAL, LAST_NAME_PERSONAL, "pleaseGenerate", PASS_PERSONAL, STREET_PERSONAL,
                STREET_NUMBER_PERSONAL, FLAT_NUMBER_PERSONAL, FLOOR_NUMBER_PERSONAL, PHONE_PERSONAL, "Zrenjanin");
        checkout.fillFormPickUp(personal);
        checkout.paymentSelection(3);
        checkout.shopSelection(1);
        checkout.acceptTerms();
        checkout.submitOrder();

        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderProductsTotal, cart.orderProductsTotal);
        sa.assertEquals(ty.orderTotalAmount, cart.orderProductsTotal);

        sa.assertAll();

    }

    @Test(priority = 1, description = "Normal and pick up product in cart - via pick Up")
    public void pickUpCompany() {
        driver.get(BASE_URL + url.p1);
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;
        sa.assertEquals(spp.cartCounter(), expected);

        spp.goToCart();
        cartItems = ci.getCartItems();
        sa.assertEquals(cartItems.get(index).productName, product1.productName);
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);

        cart.getBasketValues();
        sa.assertEquals(cart.orderProductsTotal, product1.productPrice);
        sa.assertTrue(cart.orderShippingTotal != 0);
        sa.assertEquals(cart.orderTotalAmount, cart.orderProductsTotal + cart.orderShippingTotal);

        cart.goToCheckoutPage();

        checkout.deliveryMethodSelection(2);
        checkout.selectUserType(2);
        Customer company = new Customer(FIRST_NAME_COMPANY, LAST_NAME_COMPANY, "pleaseGenerate", PASS_COMPANY, STREET_COMPANY,
                STREET_NUMBER_COMPANY, FLAT_NUMBER_COMPANY, FLOOR_NUMBER_COMPANY, PHONE_COMPANY, "Novi Sad", COMPANY_NAME, COMPANY_TAX);
        checkout.fillFormPickUp(company);
        checkout.paymentSelection(6);
        checkout.shopSelection(1);
        checkout.acceptTerms();
        checkout.submitOrder();

        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderProductsTotal, cart.orderProductsTotal);
        sa.assertEquals(ty.orderTotalAmount, cart.orderProductsTotal);

        sa.assertAll();
   }

}