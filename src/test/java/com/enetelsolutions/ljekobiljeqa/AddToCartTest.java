package com.enetelsolutions.ljekobiljeqa;

import java.util.List;

import org.testng.annotations.Test;

public class AddToCartTest extends InitTest {

	List<CartItem> cartItems;

	Product productPromo1;
	Product productPromo2;
	Product productPromo3;
	Product productPromo4;
	Product categoryProduct;
	Product searchProduct;
	Product sppProduct;
	Product similarProduct;
	Product wishlistProduct;

	public static int cartCounter;


	@Test (priority = 0, description="Add to cart simple product Home page / Promo services")
	public void addToCartPromoServiceSliders() {
//		driver.get(BASE_URL + url.loginURL);
//		login.signIn(TEMP_EMAIL, VALID_PASS);
//		login.signOut();
		driver.get(BASE_URL);
		int expected = home.cartCounter() + 3;
		productPromo1 = home.addToCartFromPromoService(3, 2);
		productPromo2 = home.addToCartFromPromoService(2, 2);
		productPromo3 = home.addToCartFromPromoService(1, 3);

		sa.assertEquals(home.cartCounter(), expected);

		home.goToCart();
		cartItems = ci.getCartItems();

		sa.assertEquals(cartItems.get(expected - 3).productName, productPromo1.productName);
		sa.assertEquals(cartItems.get(expected - 3).productQty, 1);
		sa.assertEquals(cartItems.get(expected - 3).productSinglePrice, productPromo1.productPrice);
		sa.assertEquals(cartItems.get(expected - 3).productTotalPrice, productPromo1.productPrice);

		sa.assertEquals(cartItems.get(expected - 2).productName, productPromo2.productName);
		sa.assertEquals(cartItems.get(expected - 2).productQty, 1);
		sa.assertEquals(cartItems.get(expected - 2).productSinglePrice, productPromo2.productPrice);
		sa.assertEquals(cartItems.get(expected - 2).productTotalPrice, productPromo2.productPrice);

		sa.assertEquals(cartItems.get(expected - 1).productName, productPromo3.productName);
		sa.assertEquals(cartItems.get(expected - 1).productQty, 1);
		sa.assertEquals(cartItems.get(expected - 1).productSinglePrice, productPromo3.productPrice);
		sa.assertEquals(cartItems.get(expected - 1).productTotalPrice, productPromo3.productPrice);


		sa.assertAll();
	}


	@Test (priority = 1, description="Add to cart from promo service page")
	public void addToCartPromoServicePage() {
		driver.get(BASE_URL);
		int expected = home.cartCountIncrement();
		String promoTitle = home.showAllPromo(1);
		sa.assertEquals(pspage.getPageTitle(), promoTitle);
		productPromo4 = pspage.addToCart(9);
		cartCounter = pspage.cartCounter();
		sa.assertEquals(cartCounter, expected);
		int index = cartCounter - 1;
		pspage.goToCart();
		cartItems = ci.getCartItems();

		sa.assertEquals(cartItems.get(index).productName, productPromo4.productName);
		sa.assertEquals(cartItems.get(index).productQty, 1);
		sa.assertEquals(cartItems.get(index).productSinglePrice, productPromo4.productPrice);
		sa.assertEquals(cartItems.get(index).productTotalPrice, productPromo4.productPrice);
		
		sa.assertAll();
	}

	

	@Test (priority = 2, description="Add to cart from category page")
	public void addToCartCategoryPage() {
		driver.get(BASE_URL + url.CATEGORY_URL1);
		int expected = category.cartCountIncrement();

		categoryProduct = category.addToCart(1);
		cartCounter = category.cartCounter();
		int index = cartCounter - 1;
		sa.assertEquals(cartCounter, expected);
		category.goToCart();

		cartItems = ci.getCartItems();
		sa.assertEquals(cartItems.get(index).productName, categoryProduct.productName);
		sa.assertEquals(cartItems.get(index).productQty, 1);
		sa.assertEquals(cartItems.get(index).productSinglePrice, categoryProduct.productPrice);
		sa.assertEquals(cartItems.get(index).productTotalPrice, categoryProduct.productPrice);


		sa.assertAll();

	}


	@Test (priority = 3, description="Add to cart from search result page")
	public void addToCartSearchResults() {

		category.search("smilje");
		int expected = category.cartCountIncrement();
		searchProduct = category.addToCart(3);
		cartCounter = category.cartCounter();
		int index = cartCounter - 1;
		sa.assertEquals(cartCounter, expected);
		category.goToCart();

		cartItems = ci.getCartItems();
		sa.assertEquals(cartItems.get(index).productName, searchProduct.productName);
		sa.assertEquals(cartItems.get(index).productQty, 1);
		sa.assertEquals(cartItems.get(index).productSinglePrice, searchProduct.productPrice);
		sa.assertEquals(cartItems.get(index).productTotalPrice, searchProduct.productPrice);

		sa.assertAll();
	}



	@Test (priority = 4, description="Add to cart from single product page")
	public void addToCartSingleProductPage() {

		driver.get(BASE_URL + url.p1);
		int expected = category.cartCountIncrement();
		sppProduct = spp.addToCartFromSPP();
		cartCounter = spp.cartCounter();
		int index = cartCounter - 1;
		sa.assertEquals(cartCounter, expected);
		spp.goToCart();

		cartItems = ci.getCartItems();
		sa.assertEquals(cartItems.get(index).productName, sppProduct.productName);
		sa.assertEquals(cartItems.get(index).productQty, 1);
		sa.assertEquals(cartItems.get(index).productSinglePrice, sppProduct.productPrice);
		sa.assertEquals(cartItems.get(index).productTotalPrice, sppProduct.productPrice);


		sa.assertAll();
	}



	@Test (priority = 5, description="Add to cart from single product page, similar")
	public void addToCartSppSimilarSlider() {
		int expected = home.cartCountIncrement();
		driver.get(BASE_URL + url.p1);
		similarProduct = spp.addToCartFromSimilar(3);
		cartCounter = spp.cartCounter();
		int index = cartCounter - 1;
		sa.assertEquals(cartCounter, expected);

		spp.goToCart();

		cartItems = ci.getCartItems();
		sa.assertEquals(cartItems.get(index).productName, similarProduct.productName);
		sa.assertEquals(cartItems.get(index).productQty, 1);
		sa.assertEquals(cartItems.get(index).productSinglePrice, similarProduct.productPrice);
		sa.assertEquals(cartItems.get(index).productTotalPrice, similarProduct.productPrice);

		sa.assertAll();
	}



	@Test(priority = 6, description="Add to cart from wishlist")
	public void addToCartWishlist() {
		driver.get(BASE_URL + url.loginURL);
		login.signIn(TEMP_EMAIL, VALID_PASS); 
		login.signOut();
		driver.get(BASE_URL + url.loginURL);
		login.signIn(WL_EMAIL, VALID_PASS);
		int expected = profile.cartCountIncrement();
		profile.goToWishlist();
		wishlistProduct = wl.addToCart(1);
		cartCounter = profile.cartCounter();
		int index = cartCounter - 1;
		sa.assertEquals(cartCounter, expected);

		profile.goToCart();

		cartItems = ci.getCartItems();
		sa.assertEquals(cartItems.get(index).productName, wishlistProduct.productName);
		sa.assertEquals(cartItems.get(index).productQty, 1);
		sa.assertEquals(cartItems.get(index).productSinglePrice, wishlistProduct.productPrice);
		sa.assertEquals(cartItems.get(index).productTotalPrice, wishlistProduct.productPrice);

		cart.removeFromCart(1, true);
		cart.signOut();
		sa.assertAll();
	}


	
}
