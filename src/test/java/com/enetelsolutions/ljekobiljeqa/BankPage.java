package com.enetelsolutions.ljekobiljeqa;

import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class BankPage extends MainPage {

	public BankPage(WebDriver driver) {
		super(driver);
	}

	double bankTotalAmount;


	// Locators


	@FindBy(css =".title.col-md-6")
	WebElement title;

	@FindBy(css="input#pan")
	WebElement cardNumber;

	@FindBy(id="Ecom_Payment_Card_ExpDate_Month")
	WebElement expiratonMonth;

	@FindBy(id="Ecom_Payment_Card_ExpDate_Year")
	WebElement expirationYear;

	@FindBy(id="cv2")
	WebElement cvCode;

	@FindBy(id = "amount")
	WebElement total;

	@FindBy(id="plati")
	WebElement proceed;

	@FindBy(id="btnCancel")
	WebElement cancel;

	@FindBy(css = "button#proceed-button")
	WebElement proceedButton;

	public double enterBankData(String cardNum, String expMonth, String expYear, String code, String process) {
		// process - yes, no
		this.bankTotalAmount = getAmount();
		type(cardNum, cardNumber);
		monthSelection(expMonth);
		yearSelection(expYear);
		type(code, cvCode);
		if (process.equalsIgnoreCase("yes")) {
			proceed.click();
			try {
				wait.until(ExpectedConditions.visibilityOf(proceedButton));
				proceedButton.click();
			}
			catch (Exception e) {
			}
		}
		else {
			cancel.click();
		}
		return bankTotalAmount;
	}


	public double getAmount() {
		double amount = stringToDouble(total.getText());
		return amount;
	}


	public void monthSelection(String expMonth) {
		Select monthSelect = new Select(expiratonMonth);
		monthSelect.selectByValue(expMonth);
	}

	public void yearSelection(String expYear) {
		Select yearSelect = new Select(expirationYear);
		yearSelect.selectByValue(expYear);
	}

	public double stringToDouble (String s) { // vazi za format cene npr. "7483,00"
		double number = 0;
		try {
			number = Double.parseDouble(s.replaceAll("[,]", "."));
		} catch (Exception e) {
			System.out.println("Greska kod parsiranja cene proizvoda");
		}
		return number;
	}

	public boolean isAtBankPage() {
		wait.until(ExpectedConditions.visibilityOf(title));
		return title.isDisplayed();
	}


}



