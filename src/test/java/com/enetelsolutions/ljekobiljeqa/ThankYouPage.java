package com.enetelsolutions.ljekobiljeqa;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.qameta.allure.Step;


public class ThankYouPage extends MainPage {

    public ThankYouPage(WebDriver driver) {
        super(driver);
    }

    double orderProductsTotal;
    // double orderDiscount;
    double orderShippingTotal;
    double orderTotalAmount;
    String customerFirstName;
    String customerLastName;
    String customerEmail;
    String customerStreet;
    String customerStreetnumber;
    String customerFlat;
    String customerFloor;
    String customerPhone;
    String customerCity;
    String customerPostCode;

    @FindBy(css = "div.lje_info_col_box :nth-of-type(2)")
    List <WebElement> customerData;


    @FindBy(className = "lje_thank-you-page-status")
    WebElement thankYouTitle;

    @FindBy(className = "lje_page-title")
    WebElement orderSpec;

    @FindBy(css = ".lje_basket_item_description a")
    List<WebElement> itemName;

    @FindBy(className = "lje_basket_item_description_bigness_box_totalprice")
    List<WebElement> itemTotalPrice;




    // Total

    @FindBy(css = ".lje_basket_total_wrapper_total_price_percent .price")
    List <WebElement> discount;

    @FindBy(css = ".lje_basket_total_wrapper_total_price .price")
    List <WebElement> totals;

    @FindBy(css = ".lje_basket_total_wrapper_total_price_total .price.total")
    WebElement orderTotal;

    public ThankYouPage getTYValues() {

        ThankYouPage ty = new ThankYouPage(driver);

        this.orderProductsTotal = decimalToDouble(totals.get(3).getText());
     //   this.orderDiscount = decimalToDouble(totals.get(4).getText());
        this.orderShippingTotal = decimalToDouble(totals.get(5).getText());
        this.orderTotalAmount = decimalToDouble(orderTotal.getText());
        this.customerFirstName = customerData.get(0).getText();
        this.customerLastName = customerData.get(1).getText();
        this.customerEmail = customerData.get(2).getText();
        this.customerStreet = customerData.get(4).getText();
        this.customerStreetnumber = customerData.get(5).getText();
        this.customerFloor = customerData.get(6).getText();
        this.customerPhone = customerData.get(3).getText().substring(1);
        this.customerCity = customerData.get(8).getText();
        this.customerPostCode = customerData.get(9).getText();


//        System.out.println("TY Order Shipping: " + orderShippingTotal);
//        System.out.println("TY Order Total Amount: " + orderTotalAmount);

        return ty;
    }





    @Step("Getting order code")
    public String getOrderSpecValue() {
        String orderCode = orderSpec.getText();
        orderCode = orderCode.substring(23);
        return orderCode;
    }




    @Step("Verify products total via items") // prolazi kroz iteme i sabira cene
    public double sumProductsTotal() {
        double productsTotal = 0;
        for (int i = 0; i < itemName.size(); i++) {
            double total = stringToDouble(itemTotalPrice.get(i).getText());
            productsTotal = productsTotal + total;
        }
        //	System.out.println("Sum Products Total: " + productsTotal);
        return productsTotal;
    }




    @Step ("Verifying Thank You Page")
    public boolean isAtThankYouPage() {
		wait.until(ExpectedConditions.visibilityOf(orderSpec));
		try {
            System.out.print(getOrderSpecValue() + ", ");
        }
		catch (Exception e) {

        }
		return orderSpec.isDisplayed();
	}


}