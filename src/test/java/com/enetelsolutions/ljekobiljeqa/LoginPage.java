package com.enetelsolutions.ljekobiljeqa;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.github.cdimascio.dotenv.Dotenv;
import io.qameta.allure.Step;

public class LoginPage extends MainPage{

	Dotenv dotenv = Dotenv.load();
	
	public String BASE_URL = dotenv.get("URL_B2C");
	public String WL_EMAIL = dotenv.get("WL_EMAIL");
	public String TEMP_EMAIL = dotenv.get("TEMP_EMAIL");
	public String VALID_PASS = dotenv.get("PASS");
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}

	String blankEmailMsg = "'email' adresa je obavezna";
	String blankPassMsg = "'lozinka' je obavezna";
	String invalidEmailMsg = "'email' adresa nije validna!";
	String invalidPassMsg = "'lozinka' mora sadržati minimum 6 karaktera";


	@FindBy(css = ".lje_login_inputs_wrapper input[name='email']")
	WebElement email;
	
	@FindBy(css = ".lje_login_inputs_wrapper input[name='password']")
	WebElement password;
	
	@FindBy(css=".lje_login_action [name=Submit]")
	WebElement loginButton;

	@FindBy(css=".lje_forgotten_password a")
	WebElement passResetLink;



	// Errors

	@FindBy(id = "email-error")
	WebElement emailMessageError;

	@FindBy(id = "password-error")
	WebElement passwordMessageError;


	// Register area

	@FindBy(css=".lje_login_action_register_redirect span")
	WebElement registerLink;
	

	// INFO page

	@FindBy(className = "lje_info-message-paragraph")
	WebElement infoMessage;

	@FindBy(css=".lje_info-message-paragraph a")
	List <WebElement> clickHereLink;



	// PASS RESET page

	@FindBy(css=".lje_login_inputs_wrapper input[name='email']")
	WebElement emailField;

	@FindBy(css=".lje_login_action .btn--primary")
	WebElement sendBtn;



	// Methods area

	@Step("Signing in...")
	public void signIn(String mail, String pass) {
		type(mail,email);
			type(pass,password);
			loginButton.click();
	}

	@Step("Go to register form")
	public void goToRegisterForm() {
		registerLink.click();
	}

	@Step("Go to pass reset via login page")
	public void goToPassResetPage() {
		passResetLink.click();
	}


	@Step("Getting error in email input area")
	public String getEmailError() {
		return emailMessageError.getText();
	}
	
	@Step("Getting error in password input area")
	public String getPassError() {
		return passwordMessageError.getText();
	}

	@Step("Verifying login page is opened")
	public boolean isAtLoginPage() {
		try {
			wait.until(ExpectedConditions.visibilityOf(loginButton));
			return loginButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
		
		
	}

	// INFO PAGE

	@Step("Trying again at wrong login input parameters")
	public void tryAgain() {
		sleep(1);
		clickHereLink.get(0).click();
	}

	@Step("Getting password reset page")
	public void getpassReset() {
		clickHereLink.get(1).click();
	}

	@Step("Verifying login page is opened")
	public boolean isAtInfoLoginPage() {
		wait.until(ExpectedConditions.visibilityOf(infoMessage));
		return infoMessage.isDisplayed();
	}

	
	// PASS RESET PAGE
	
	@Step("Getting password reseted")
	public void passReset(String email) {
		type(email, emailField);
		sendBtn.click();
	}

	@Step("Signing in Temporary User to clear basket items")
	public void recycleLogin() {
	LoginPage login = new LoginPage(driver);
	goToProfile();
	login.signIn(TEMP_EMAIL, VALID_PASS);
	login.signOut();
	}


}
