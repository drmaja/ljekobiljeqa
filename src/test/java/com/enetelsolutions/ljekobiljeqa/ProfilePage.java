package com.enetelsolutions.ljekobiljeqa;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;

public class ProfilePage extends MainPage {

	public ProfilePage(WebDriver driver) {
		super(driver);
	}
	
	// Profile menu items

	@FindBy(css = ".lje_account-menu-item a")
	List <WebElement> profileLinks;

	/*
	0 - Podaci
	1 - Kupovine
	2 - Lozinka
	 */

	@FindBy(css = ".lje_profile_logout a")
	WebElement logOut;


	// Methods

	@Step("Go to personal data")
	public void getPersonalData() {
		profileLinks.get(0).click();
	}
	
	@Step("Go to orders")
	public void getMyOrders() {
		profileLinks.get(1).click();
	}

	@Step("Go to password change")
	public void getPassChange() {
		profileLinks.get(2).click();
	}
	

	
	@Step("Log out")
	public void getLogOut() {
		logOut.click();
	}




}
