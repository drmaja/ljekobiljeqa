package com.enetelsolutions.ljekobiljeqa;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.qameta.allure.Step;

public class HomePage extends MainPage {

	public HomePage(WebDriver driver) {
		super(driver);
	}

	JavascriptExecutor js = (JavascriptExecutor) driver;

	// Locators

	// Promo services

	
	@FindBy(css = ".lje_promo_service_1 .slick-track")
	WebElement promoSlider1;

	@FindBy(css = ".lje_promo_service_2 .slick-track")
	WebElement promoSlider2;

	@FindBy(css = ".lje_promo_service_3 .slick-track")
	WebElement promoSlider3;

	@FindBy(css = ".main-banner")
	WebElement mainBanner;


	// Product image


	@FindBy(css = ".lje_promo_service_1 .slick-track .slick-active .lje_product_wrapper_image")
	List<WebElement> productImagePS1;

	@FindBy(css = ".lje_promo_service_2 .slick-track .slick-active .lje_product_wrapper_image")
	List<WebElement> productImagePS2;

	@FindBy(css = ".lje_promo_service_3 .slick-track .slick-active .lje_product_wrapper_image")
	List<WebElement> productImagePS3;


	// Add to cart btn

	@FindBy(css = ".lje_promo_service_1 .slick-track .slick-active .buy-btn")
	List<WebElement> addToCartPS1;

	@FindBy(css = ".lje_promo_service_2 .slick-track .slick-active .buy-btn")
	List<WebElement> addToCartPS2;

	@FindBy(css = ".lje_promo_service_3 .slick-track .slick-active .buy-btn")
	List<WebElement> addToCartPS3;


	// Product names

	@FindBy(css = ".lje_promo_service_1 .slick-active .lje_product_wrapper_description_name")
	List<WebElement> productNamePS1;

	@FindBy(css = ".lje_promo_service_2 .slick-active .lje_product_wrapper_description_name")
	List<WebElement> productNamePS2;

	@FindBy(css = ".lje_promo_service_3 .slick-active .lje_product_wrapper_description_name")
	List<WebElement> productNamePS3;

	// Product prices

	@FindBy(css = ".lje_promo_service_1 .slick-track .slick-active .lje_product_wrapper_description_newprice")
	List<WebElement> productPricePS1;

	@FindBy(css = ".lje_promo_service_2 .slick-track .slick-active .lje_product_wrapper_description_newprice")
	List<WebElement> productPricePS2;

	@FindBy(css = ".lje_promo_service_3 .slick-track .slick-active .lje_product_wrapper_description_newprice")
	List<WebElement> productPricePS3;



	// Promo titles
	@FindBy(className = "lje_promo_service_title")
	List<WebElement> promoTitles;

	// Show all from promo
	@FindBy(css = ".lje_promo_service_action a")
	List<WebElement> showAllPromo;



	// Methods



	@Step("Adding to cart from promo services")
	public Product addToCartFromPromoService(int ps, int index) {
		index = index - 1;
		Product product = new Product();
		if (ps == 1) {
			js.executeScript("arguments[0].scrollIntoView(true)", promoSlider1);
			sleep(1);
			if (index <= addToCartPS1.size()) {
//				sleep(1);
				hover(productImagePS1.get(index));
				addToCartPS1.get(index).click();
				product.productName = productNamePS1.get(index).getText();
				product.productPrice = decimalToDouble(productPricePS1.get(index).getText()) ;
			}
			else {
				System.out.println("You have entered wrong index number");
			} }
		else if (ps == 2) {
			js.executeScript("arguments[0].scrollIntoView(true)", promoSlider2);
			sleep(1);
			if (index <= addToCartPS2.size()) {
//				sleep(1);
				hover(productImagePS2.get(index));
				addToCartPS2.get(index).click();
				product.productName = productNamePS2.get(index).getText();
				product.productPrice = decimalToDouble(productPricePS2.get(index).getText());
			}
			else {
				System.out.println("You have entered wrong index number");
			} }
		else if (ps == 3) {
			js.executeScript("arguments[0].scrollIntoView(true)", promoSlider3);
			sleep(1);
			if (index <= addToCartPS3.size()) {
//				sleep(1);
				hover(productImagePS3.get(index));
				addToCartPS3.get(index).click();
				product.productName = productNamePS3.get(index).getText();
				product.productPrice = decimalToDouble(productPricePS3.get(index).getText());
			}
			else {
				System.out.println("You have entered wrong index number");
			} }
		else {
			System.out.println("Wrong promo service index number");
		}
		return product;
	}

		@Step("Accessing promo service page")
		public String showAllPromo(int index) {
			index = index - 1;
			String promoTitle = "";



			if (index <= showAllPromo.size()) {

				try {
					// scroll needs update, working only for 1st promo service
					js.executeScript("arguments[0].scrollIntoView(true)", mainBanner);
					promoTitle = promoTitles.get(index).getText();
					showAllPromo.get(index).click();

				} catch (Exception e) {
					System.out.println("Cannot access promo service");
				}
			} else {
				System.out.println("Ooops, wrong promo service index number");
				}
			return promoTitle;
		}

		@Step("Verifikacija pocetne stranice")
		public boolean isAtHomePage() {
			wait.until(ExpectedConditions.visibilityOf(promoSlider1));
			return promoSlider1.isDisplayed();
		}


		
}


