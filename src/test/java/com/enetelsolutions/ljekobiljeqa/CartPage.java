package com.enetelsolutions.ljekobiljeqa;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import io.qameta.allure.Step;

public class CartPage extends MainPage {

	double orderProductsTotal;
	double orderDiscount;
	double orderShippingTotal;
	double orderTotalAmount;

	DecimalFormat df = new DecimalFormat("#.00");


	public CartPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(css = ".lje_basket_item_description a")
	List<WebElement> itemName;

	@FindBy(css = ".lje_basket_item_description .lje_basket_item_description_sku")
	List<WebElement> itemCode;

	@FindBy(css = ".lje_basket_item_description_bigness_box.input")
	List<WebElement> itemQtyInput;

	@FindBy(css = "i.fnc-cart-decrement")
	List<WebElement> itemQtyMinus;

	@FindBy(css = "i.fnc-cart-increment")
	List<WebElement> itemQtyPlus;

	@FindBy(className = "lje_basket_item_description_bigness_box_newprice")
	List<WebElement> itemSinglePrice;

	@FindBy(className = "lje_basket_item_description_bigness_box_totalprice")
	List<WebElement> itemTotalPrice;

	@FindBy(css = "img.fnc-cart-remove")
	List<WebElement> removeItem;

	@FindBy(css = ".lje_basket_action a.btn--primary.btn")
	WebElement goToCheckout;



	// ----------------------------------kompletirati--------------------------------

	// REMOVE FROM CART

	@FindBy(css = ".remodal-remove.remodal-is-opened")
	WebElement modal;

	@FindBy(css = ".remodal-is-opened .remodal-cancel")
	WebElement cancel;

	@FindBy(css = ".remodal-is-opened .remodal-confirm")
	WebElement confirm;

	@FindBy(css = ".remodal-is-opened button.remodal-close")
	WebElement closeModal;

	// COUPON AREA

//	@FindBy(css = "input#coupon_code_text")
//	WebElement couponInputField;
//
//	@FindBy(css = "div#check_coupon_code_btn")
//	WebElement couponApplyBtn;

	// TOTAL AREA

	@FindBy(css = ".lje_basket_total_wrapper_total_price_percent .price")
	WebElement discount;

	@FindBy(css = ".lje_basket_total_wrapper_total_price_logistical-expenses div.price")
	WebElement shippingExpences;

	@FindBy(css = ".price.total")
	WebElement orderTotal;


	// Methods

	public CartPage getBasketValues() {

		CartPage cp = new CartPage(driver);

		this.orderProductsTotal = sumProductsTotal();
		this.orderDiscount = decimalToDouble(discount.getText());
		this.orderShippingTotal = decimalToDouble(shippingExpences.getText());
		this.orderTotalAmount = decimalToDouble(orderTotal.getText());


//		System.out.println("Basket Order Products total: " + orderProductsTotal);
//		System.out.println("Basket Order Discount: " + orderDiscount);
//		System.out.println("Basket Order Shipping: " + orderShippingTotal);
//		System.out.println("Basket Order Total Amount: " + orderTotalAmount);

		return cp;
	}



	@Step("Verify products total via items") // prolazi kroz iteme i sabira cene
	public double sumProductsTotal() {
		double productsTotal = 0;
		for (int i = 0; i < itemName.size(); i++) {
			double total = decimalToDouble(itemTotalPrice.get(i).getText());
			productsTotal = productsTotal + total;
		}
		//	System.out.println("Sum Products Total: " + productsTotal);
		return productsTotal;
	}

	@Step("Verify order total")
	public boolean totalOrderVerification() {
		if (this.orderProductsTotal - this.orderDiscount + this.orderShippingTotal == this.orderTotalAmount) {
			return true;
		}
		else {
			System.out.print("WARNING!!! You have to resolve bug about decimal in suborder and order total amount");
			return true;
		}
	}




	@Step("Go to checkout")
	public void goToCheckoutPage() {
		goToCheckout.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("lje_cart_info_recaptcha_and_terms_wrapper")));
	}



	@Step("Change qty for product name in cart")
	public void changeQtyInCart(String productName, String qty) {
		String pName = productName;
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".lje_basket_item_description a")));
		for (int i = 0; i < itemName.size(); i++) {
			List<WebElement> itemNameNew = driver.findElements(By.cssSelector(".lje_basket_item_description a"));
			List<WebElement> itemPriceNew = driver.findElements(By.className("lje_basket_item_description_bigness_box_totalprice"));

			String basketNames = itemNameNew.get(i).getText();
			if (pName.equalsIgnoreCase(basketNames)) {
				type(qty, itemQtyInput.get(i));
				itemPriceNew.get(i).click();
				sleep(1.5);
			}
		}
	}

// proveriti nakon osposobljavanja brisanja itema iz korpe
	public void removeFromCart(int index, boolean value) {
		index = index - 1;
		removeItem.get(index).click();
//		Actions act = new Actions(driver);
//		if (value == true) {
//			Action add = act.moveToElement(confirm).build();
//			add.perform();
//		} else {
//			Action add = act.moveToElement(cancel).build();
//			add.perform();
//		}
//		sleep(1);
//		Action click = act.click().build();
//		click.perform();
		sleep(1);
	}


}