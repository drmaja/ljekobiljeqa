package com.enetelsolutions.ljekobiljeqa;

import org.testng.annotations.Test;

import io.qameta.allure.Step;


public class LoginTest extends InitTest {


	
	@Step("Happy path case")
	@Test(priority = 0, description="Happy path case") // HAPPY PATH
		public void login01() {
		driver.get( BASE_URL + url.loginURL);
		login.signIn(VALID_EMAIL, VALID_PASS);
		sa.assertTrue(login.isSignedIn());
		login.signOut();
		
		sa.assertAll();
	}

	@Step("Blank email and password")
	@Test(priority = 1, description="Blank email and password", dependsOnMethods = "login01")
	public void login02() {
		driver.get( BASE_URL + url.loginURL);
		login.signIn(BLANK, BLANK);
		sa.assertTrue(login.isAtLoginPage());
		sa.assertEquals(login.getEmailError(), login.blankEmailMsg);
		sa.assertEquals(login.getPassError(), login.blankPassMsg);
		
		sa.assertAll();
	}

	@Step("Uncorrect email and valid password")
	@Test(priority = 2, description="Uncorrect email and valid password", dependsOnMethods = "login01")
	public void login03() {
		login.signIn(UNCORRECT_EMAIL, VALID_PASS);
		sa.assertTrue(login.isAtInfoLoginPage());
		login.tryAgain();
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}

	@Step("Invalid no monkey email and valid password")
	@Test(priority = 3, description="Invalid no monkey email and valid password", dependsOnMethods = "login01")
	public void login04() {
		login.signIn(INVALID_NO_MONKEY_EMAIL, VALID_PASS);
		sa.assertEquals(login.getEmailError(), login.invalidEmailMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}


	@Step("Invalid no dot email and valid password")
	@Test(priority = 4, description="Invalid no dot email and valid password", dependsOnMethods = "login01")
	public void login05() {

		login.signIn(INVALID_NO_DOT_EMAIL, VALID_PASS);
		sa.assertTrue(login.isAtInfoLoginPage());
		login.tryAgain();
		
		sa.assertAll();
	}

	@Step("Invalid with space email and valid password")
	@Test(priority = 5, description="Invalid with space email and valid password", dependsOnMethods = "login01")
	public void login06() {
		login.signIn(INVALID_WITH_SPACE_EMAIL, VALID_PASS);
		sa.assertEquals(login.getEmailError(), login.invalidEmailMsg);
		sa.assertTrue(login.isAtLoginPage());

		sa.assertAll();
	}

	@Step("Blank email and valid password")
	@Test(priority = 6, description="Blank email and valid password", dependsOnMethods = "login01")
	public void login07() {
		login.signIn(BLANK, VALID_PASS);
		sa.assertEquals(login.getEmailError(), login.blankEmailMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}

	@Step("Valid email and wrong password")
	@Test(priority = 6, description="Valid email and wrong password", dependsOnMethods = "login01")
	public void login08() {
		login.signIn(VALID_EMAIL, UNCORRECT_PASS);
		sa.assertTrue(login.isAtInfoLoginPage());
		login.tryAgain();
		
		sa.assertAll();
	}

	@Step("Valid email and short invalid password")
	@Test(priority = 7, description="Valid email and short invalid password", dependsOnMethods = "login01")
	public void login09() {
		login.signIn(VALID_EMAIL, INVALID_SHORT_PASS);
		sa.assertEquals(login.getPassError(), login.invalidPassMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}

	@Step("Valid email and blank password")
	@Test(priority = 8, description="Valid email and blank password", dependsOnMethods = "login01")
	public void login10() {
		login.signIn(VALID_EMAIL, BLANK);
		sa.assertEquals(login.getPassError(), login.blankPassMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}

	@Step("Valid email and invalid password with space")
	@Test(priority = 9, description="Valid email and invalid password with space", dependsOnMethods = "login01")
	public void login11() {
		login.signIn(VALID_EMAIL, INVALID_WITH_SPACE_PASS);
		sa.assertTrue(login.isAtInfoLoginPage());
		login.tryAgain();
		
		sa.assertAll();
	}

	@Step("Valid email in password input and valid password in email input field")
	@Test(priority = 10, description="Valid email in password input and valid password in email input field", dependsOnMethods = "login01")
	public void login12() {
		login.signIn(VALID_PASS, VALID_EMAIL);
		sa.assertEquals(login.getEmailError(), login.invalidEmailMsg);
		sa.assertTrue(login.isAtLoginPage());
		
		sa.assertAll();
	}


}
