package com.enetelsolutions.ljekobiljeqa;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;

public class SingleProductPage extends MainPage {

	public SingleProductPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(className = "product-info__title")
	WebElement productName;

	@FindBy(className = "product-info__code")
	WebElement productCode;

	@FindBy(className = "product-info__price__old")
	WebElement productOldPrice;

	@FindBy(className = "product-info__price__new")
	WebElement productPrice;

	@FindBy(css = ".sizes-list li")
	List <WebElement> productAttribute;

	@FindBy(className = "product-quantity__input")
	WebElement quantity;

	@FindBy(css = "a.fnc-cart-btn")
	WebElement addToCart;

	@FindBy(css = "a.btn--wish-list")
	WebElement addToWishlist;

	@FindBy(className = "lje_promo_service_title")
	WebElement titleSimilar;


	// Similar products

	// Add to cart btn

	@FindBy(css = ".slick-active .buy-btn")
	List<WebElement> addToCartSimilar;

	@FindBy(css = ".slick-active .lje_product_wrapper_description_name")
	List<WebElement> productNameSimilar;

	@FindBy(css = ".slick-active .lje_product_wrapper_description_newprice")
	List<WebElement> productPriceSimilar;


	// Methods

	@Step("Adding to cart from spp")
	public Product addToCartFromSPP () {
		product = new Product();
		product.productCode = productCode.getText(); // kod proizvoda
		product.productName = productName.getText(); // ime proizvoda
		product.productName = product.productName.toUpperCase(); // zbog bug-a sa kapitalizacijom
		product.productPrice = decimalToDouble(productPrice.getText()); // cena proizvoda
		addToCart.click();
		sleep(0.5);
		return product;
	}

	@Step("Change product attribute")
	public void changeAttribute(int index) {
		index = index - 1;

	}


	@Step("Adding to cart Similar")
		public Product addToCartFromSimilar(int index) {
			index = index - 1;
			product = new Product();
			if (index <= addToCartSimilar.size()) {
					sleep(1);
					hover(addToCartSimilar.get(index));
					addToCartSimilar.get(index).click();
					product.productName = productNameSimilar.get(index).getText();
					product.productPrice = stringToDouble(productPriceSimilar.get(index).getText());
			} else {
				System.out.println("You have entered wrong index number");
			}
			return product;
		}


	
}
