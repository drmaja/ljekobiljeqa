package com.enetelsolutions.ljekobiljeqa;

import org.testng.annotations.Test;

import java.util.List;

public class ShoppingDeliveryToAddressTest extends InitTest {

    List<CartItem> cartItems;
    Product product1;
    Product product2;


   @Test (priority = 0, description="Normal product in cart - via account, shipping > 0")
	public void shopping01RegularProductViaAccount() {
        driver.get(BASE_URL + url.p3);
        spp.cookieAgree();
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;
        sa.assertEquals(spp.cartCounter(), expected);

        spp.goToCart();
        cartItems = ci.getCartItems();
        sa.assertEquals(cartItems.get(index).productName, product1.productName);
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);

        cart.getBasketValues();
        sa.assertEquals(cart.orderProductsTotal, product1.productPrice);
        sa.assertTrue(cart.orderShippingTotal != 0);
        sa.assertEquals(cart.orderTotalAmount, cart.orderProductsTotal + cart.orderShippingTotal);


        cart.goToCheckoutPage();
        Customer personal = new Customer(FIRST_NAME_PERSONAL, LAST_NAME_PERSONAL, "pleaseGenerate", PASS_PERSONAL, STREET_PERSONAL,
               STREET_NUMBER_PERSONAL, FLAT_NUMBER_PERSONAL, FLOOR_NUMBER_PERSONAL, PHONE_PERSONAL, "Temerin");
        checkout.fillForm(personal);
        checkout.citySelect("Tem");
        checkout.paymentSelection(3);
        checkout.acceptTerms();
        checkout.submitOrder();
        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.customerFirstName, personal.getFirstName());
        sa.assertEquals(ty.customerLastName, personal.getLastName());
        sa.assertEquals(ty.customerEmail, personal.getEmail());
        sa.assertEquals(ty.customerStreet, personal.getStreet());
        sa.assertEquals(ty.customerStreetnumber, personal.getStreetnumber());
        sa.assertEquals(ty.customerFloor, personal.getFloor());
        sa.assertEquals(ty.customerPhone, personal.getPhone());
        sa.assertEquals(ty.customerCity, personal.getCity());

        sa.assertEquals(ty.orderProductsTotal, cart.orderProductsTotal);
        sa.assertEquals(ty.orderShippingTotal, cart.orderShippingTotal);
        sa.assertEquals(ty.orderTotalAmount, cart.orderTotalAmount);

		sa.assertAll();
	}

    @Test (priority = 1, description="Two products in cart - via Upon Delivery")
    public void shopping02MixedProductsViaUponDelivery() {
         driver.get(BASE_URL + url.p1);
         int expected = spp.cartCountIncrement();
         product1 = spp.addToCartFromSPP();
         int index = expected - 1;
         sa.assertEquals(spp.cartCounter(), expected);

         spp.goToCart();
         cartItems = ci.getCartItems();
         sa.assertEquals(cartItems.get(index).productName, product1.productName);
         sa.assertEquals(cartItems.get(index).productQty, 1);
         sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
         sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);

         cart.getBasketValues();
         sa.assertEquals(cart.orderProductsTotal, product1.productPrice);
         sa.assertTrue(cart.orderShippingTotal != 0);
         sa.assertEquals(cart.orderTotalAmount, cart.orderProductsTotal + cart.orderShippingTotal);

         driver.get(BASE_URL + url.p3);
         int expected2 = spp.cartCountIncrement();
         product2 = spp.addToCartFromSPP();
         int index2 = expected2 - 1;
         sa.assertEquals(spp.cartCounter(), expected2);

         spp.goToCart();
         cartItems = ci.getCartItems();
         sa.assertEquals(cartItems.get(index2).productName, product2.productName);
         sa.assertEquals(cartItems.get(index2).productQty, 1);
         sa.assertEquals(cartItems.get(index2).productSinglePrice, product2.productPrice);
         sa.assertEquals(cartItems.get(index2).productTotalPrice, product2.productPrice);

         cart.getBasketValues();
         sa.assertEquals(cart.orderProductsTotal, product1.productPrice + product2.productPrice);
         sa.assertTrue(cart.orderShippingTotal == 0);
         sa.assertEquals(cart.orderTotalAmount, cart.orderProductsTotal + cart.orderShippingTotal);


         cart.goToCheckoutPage();
         Customer personal = new Customer(FIRST_NAME_PERSONAL, LAST_NAME_PERSONAL, "pleaseGenerate", PASS_PERSONAL, STREET_PERSONAL,
                STREET_NUMBER_PERSONAL, FLAT_NUMBER_PERSONAL, FLOOR_NUMBER_PERSONAL, PHONE_PERSONAL, "Temerin");
         checkout.fillForm(personal);
         checkout.citySelect("Tem");
         checkout.paymentSelection(2);
         checkout.acceptTerms();
         checkout.submitOrder();
         sa.assertTrue(ty.isAtThankYouPage());
         ty.getTYValues();
         sa.assertEquals(ty.orderProductsTotal, cart.orderProductsTotal);
         sa.assertEquals(ty.orderShippingTotal, cart.orderShippingTotal);
         sa.assertEquals(ty.orderTotalAmount, cart.orderTotalAmount);

         sa.assertAll();
    }

    @Test (priority = 2, description="Normal product in cart - via Bank card")
    public void shopping03PaymentByCard() {
        driver.get(BASE_URL + url.p2);
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;

        sleep(2);
        spp.goToCart();
        cartItems = ci.getCartItems();
        sa.assertEquals(cartItems.get(index).productName, product1.productName);
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);


        cart.getBasketValues();
        sa.assertEquals(cart.orderProductsTotal, product1.productPrice);
        sa.assertTrue(cart.orderShippingTotal != 0);
        sa.assertEquals(cart.orderTotalAmount, cart.orderProductsTotal + cart.orderShippingTotal);

        cart.goToCheckoutPage();
        Customer personal2 = new Customer(FIRST_NAME, LAST_NAME, EMAIL, PASS, STREET,
                STREET_NUMBER, FLAT_NUMBER, FLOOR_NUMBER, PHONE, "Temerin");
        checkout.fillForm(personal2);
        checkout.citySelect("Tem");
        checkout.paymentSelection(1);
        checkout.acceptTerms();
        checkout.submitOrder();
        sa.assertTrue(bank.isAtBankPage());
        bank.enterBankData(BANK_CARD, EXP_MONTH, EXP_YEAR, BANK_CODE, "yes");
        sa.assertEquals(bank.bankTotalAmount, cart.orderTotalAmount);
        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderProductsTotal, cart.orderProductsTotal);
        sa.assertEquals(ty.orderShippingTotal, cart.orderShippingTotal);
        sa.assertEquals(ty.orderTotalAmount, cart.orderTotalAmount);
        // validacija da je placanje uspesno, a ne neuspesno
        sa.assertAll();
    }

    @Test (priority = 3, description="Normal product in cart - company")
    public void shopping04Company() {
        driver.get(BASE_URL + url.p1);
        int expected = spp.cartCountIncrement();
        product1 = spp.addToCartFromSPP();
        int index = expected - 1;
        sa.assertEquals(spp.cartCounter(), expected);

        spp.goToCart();
        cartItems = ci.getCartItems();
        sa.assertEquals(cartItems.get(index).productName, product1.productName);
        sa.assertEquals(cartItems.get(index).productQty, 1);
        sa.assertEquals(cartItems.get(index).productSinglePrice, product1.productPrice);
        sa.assertEquals(cartItems.get(index).productTotalPrice, product1.productPrice);

        cart.getBasketValues();
        sa.assertEquals(cart.orderProductsTotal, product1.productPrice);
        sa.assertTrue(cart.orderShippingTotal != 0);
        sa.assertEquals(cart.orderTotalAmount, cart.orderProductsTotal + cart.orderShippingTotal);


        cart.goToCheckoutPage();
        checkout.selectUserType(2);
        Customer company = new Customer(FIRST_NAME_COMPANY, LAST_NAME_COMPANY, "pleaseGenerate", PASS_COMPANY, STREET_COMPANY,
                STREET_NUMBER_COMPANY, FLAT_NUMBER_COMPANY, FLOOR_NUMBER_COMPANY, PHONE_COMPANY, "Temerin", COMPANY_NAME, COMPANY_TAX);
        checkout.fillForm(company);
        checkout.citySelect("Tem");
        checkout.paymentSelection(6);
        checkout.acceptTerms();
        checkout.submitOrder();
        sa.assertTrue(ty.isAtThankYouPage());
        ty.getTYValues();
        sa.assertEquals(ty.orderProductsTotal, cart.orderProductsTotal);
        sa.assertEquals(ty.orderShippingTotal, cart.orderShippingTotal);
        sa.assertEquals(ty.orderTotalAmount, cart.orderTotalAmount);

        sa.assertAll();
    }

}