package com.enetelsolutions.ljekobiljeqa;

import java.util.List;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class CategoryPage extends MainPage {

	public CategoryPage(WebDriver driver) {
		super(driver);
	}
	
	
	@FindBy (css = ".category-title h1")
	WebElement categoryTitle;

	
	// Sorting zone
	
	@FindBy (css = ".filters__type.filters__type--sort .filters__list--item input#name")
	WebElement sortByName;

	@FindBy (css=".filters__type.filters__type--sort .filters__list--item input#price_asc")
	WebElement sortByPriceAsc;

	@FindBy (css=".filters__type.filters__type--sort .filters__list--item input#price_desc")
	WebElement sortByPriceDesc;

	// Pagination - to be defined

//	@FindBy (css=".paginationTG ul li.currentpage")
//	WebElement currentPage;
//
//	@FindBy (css=".paginationTG ul li a")
//	List <WebElement> pagerLinks;
//
//	@FindBy (linkText="SLEDEĆA")
//	WebElement goToNextPage;
//
//	@FindBy (linkText=">>")
//	WebElement goToLastPage;
	


	// Product zone (grid)


	@FindBy(css = ".lje_product_wrapper div.lje_product_wrapper_image .lje_product_wrapper_image_promo_bookmark ~ a")
	List <WebElement> productImageLink;

	@FindBy(css = ".lje_product_wrapper .lje_product_wrapper_description_name")
	List <WebElement> productName;

	@FindBy(css = ".lje_product_wrapper .lje_product_wrapper_description_newprice")
	List <WebElement> productPrice;

	@FindBy(css = ".lje_product_wrapper .lje_product_wrapper_description .fnc-cart-btn")
	List <WebElement> productAddToCart;

	@FindBy(css = ".lje_product_wrapper_image_bookmark .lje_product-wish")
	List <WebElement> saveToWishlist;



	// Methods

	// Product zone methods

	@Step("Getting product's code, name and price")
	public Product getProductData (int index) {
		product = new Product();
		product.productName = productName.get(index).getText(); // ime proizvoda
		product.productPrice = decimalToDouble(productPrice.get(index).getText()); // cena proizvoda
		return product;
	}

	@Step("Adding to cart")
	public Product addToCart (int index) {
		index = index - 1;
		product = getProductData(index);
		hover(productAddToCart.get(index));
		productAddToCart.get(index).click();
		sleep(2);
		return product;
	}

	public String getPageTitle() {
		wait.until(ExpectedConditions.visibilityOf(categoryTitle));
		String title = categoryTitle.getText();
		return title;
	}

}