package com.enetelsolutions.ljekobiljeqa;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;

public class CartItem extends CartPage {

    String productName;
	int productQty;
	double productSinglePrice;
    double productTotalPrice;
    
    List <CartItem> cartItems;


    public CartItem(WebDriver driver) {
        super(driver);
    }

    public List<CartItem> getCartItems() {
        cartItems = new ArrayList<>();
        int counter = itemCounter();

        for (int i = 0; i < counter; i++) {
            CartItem item = new CartItem(driver);
            item.productName = itemName.get(i).getText();
            String qty = itemQtyInput.get(i).getText();
            item.productQty = Integer.parseInt(qty);
            item.productSinglePrice = decimalToDouble(itemSinglePrice.get(i).getText());
            item.productTotalPrice = decimalToDouble(itemTotalPrice.get(i).getText());
//            System.out.println(productTotalPrice);
            cartItems.add(item);
        }
        return cartItems; 
    }


    public int itemCounter() {
        return itemName.size();
    }


    
}