package com.enetelsolutions.ljekobiljeqa;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.qameta.allure.Step;

public class CheckoutPage extends MainPage {

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }



    @FindBy(css="div.lje_user-type div[data-value='fizicko']")
    WebElement personalChk;

    @FindBy(css="div.lje_user-type div[data-value='pravno']")
    WebElement companyChk;

    @FindBy(css = "div.lje_pickup_wrapper div[data-value='1'] .lje_pickup-box_wrapper__images")
    WebElement delivery;

    @FindBy(css = "div.lje_pickup_wrapper div[data-value='2'] .lje_pickup-box_wrapper__images")
    WebElement pickUp;

    @FindBy(css = ".cart_shops_list .checkbox")
    List<WebElement> shopList;

    @FindBy(id="note")
    WebElement notes;

    @FindBy(css = "input#first_name")
    WebElement firstName;

    @FindBy(css = "input#last_name")
    WebElement lastName;

    @FindBy(css = "input#email-main")
    WebElement email;

    @FindBy(css = "input[name='order[mobile]']")
    WebElement phone;

    @FindBy(css = "input[name='order[street]']")
    WebElement street;

    @FindBy(css = "input[name='order[street_number]']")
    WebElement number;

    @FindBy(css = "input#floor")
    WebElement floor;

    @FindBy(css = "input[name='order[flat]']")
    WebElement flat;

    @FindBy(css = "input#city")
    WebElement city;

    @FindBy(css = "input#first_name ~ .form-error")
    WebElement firstNameError;

    @FindBy(css = "input#last_name ~ .form-error")
    WebElement lastNameError;

    @FindBy(css = "input#email-main ~ .form-error")
    WebElement emailError;

    @FindBy(css = "input[name='order[mobile]'] ~ .form-error")
    WebElement phoneError;

    @FindBy(css = "input[name='order[street]'] ~ .form-error")
    WebElement streetError;

    @FindBy(css = "input[name='order[street_number]'] ~ .form-error")
    WebElement numberStreetError;


    // Srediti nakon sredjivanja checkout stranice

//    @FindBy(id = "city-error")
//    WebElement cityError;
//
//    @FindBy(id = "post_code-error")
//    WebElement postCodeError;
//
//    String nameErrorMsg = "'Ime' je obavezno";
//    String streetErrorMsg = "'Prezime' je obavezno";
//    String emailErrorMsgInvalid = "'Email' adresa nije validna!";
//    String emailErrorMsgMissing = "'Email' adresa je obavezna";
//    String phoneErrorMsgInvalid = "'Telefon' nije validan. (381xxxxxxxxx)";
//    String phoneErrorMsgMissing = "'Telefon' je obavezan";
//    String streetErrorMsgInvalid = "'Ime ulice' nije validno";
//    String streetErrorMsgMissing = "'Ime ulice' je obavezno";
//    String numberStreetErrorMsgInvalid = "'Broj ulice' nije validan! (bb, 12a, 34b/lok3, 34/4/25,...)";
//    String numberStreetErrorMsgMissing = "'Broj ulice' je obavezan";
//    String cityErrorMsgMissing = "'Grad (mesto)' je obavezan'";
//    String postalCodeErrorMsgMissing = "'Poštanski broj' je obavezan";




    // COMPANY DATA
    @FindBy(css = "input[name='order[company_name]']")
    WebElement companyName;

    @FindBy(css = "input[name='order[pib]']")
    WebElement companyTaxNumber;

    @FindBy(css="input[name='order[maticni_broj]']")
    WebElement companyRegisterNumber;

    @FindBy(css = ".lje_cart_form_top__two div.lje_payment_type_wrapper div[data-value] .checkbox")
    List <WebElement> paymentOptions;

    @FindBy(css = ".lje_cart_info_recaptcha_and_terms_wrapper .checkbox")
    WebElement acceptTerms;

    @FindBy(css = ".lje_cart_action button[name=Submit]")
    WebElement submit;

    @FindBy(css = "a[onclick='goBack()']")
	WebElement goBackToCart;




    // Methods

    @Step("user type selection")
    public void selectUserType(int x) {
        if (x == 2) {
            companyChk.click();
        }
        else if (x == 1) {
            personalChk.click();
        }
    }


    @Step("Fill-out form")
    public void fillForm(Customer customer) {
        type(customer.getFirstName(), firstName);
        type(customer.getLastName(), lastName);
        String emailToUse = customer.getEmail();
        if (emailToUse.contains("@")) {
            customer.setEmail(emailToUse);
        } else if ((customer.getTax().length() == 9)) {
            customer.setEmail(emailAdressGenerator("p"));
        } else {
            customer.setEmail(emailAdressGenerator("f"));
        }
        type(customer.getEmail(), email);
        type(customer.getPhone(), phone);
        type(customer.getStreet(), street);
        type(customer.getStreetnumber(), number);
        type(customer.getFloor(), floor);
        type(customer.getFlat(), flat);
        if ((customer.getTax().length() == 9)) {
            type(customer.getCompanyName(), companyName);
            type(customer.getTax(), companyTaxNumber);
        }

    }

    @Step("Fill-out form")
    public void fillFormPickUp(Customer customer) {
        type(customer.getFirstName(), firstName);
        type(customer.getLastName(), lastName);
        String emailToUse = customer.getEmail();
        if (emailToUse.contains("@")) {
            customer.setEmail(emailToUse);
        } else if ((customer.getTax().length() == 9)) {
            customer.setEmail(emailAdressGenerator("p"));
        } else {
            customer.setEmail(emailAdressGenerator("f"));
        }
        type(customer.getEmail(), email);
        type(customer.getPhone(), phone);
        if ((customer.getTax().length() == 9)) {
            type(customer.getCompanyName(), companyName);
            type(customer.getTax(), companyTaxNumber);
        }

    }



    @Step("City selection")
    public void citySelect(String cityenter) {
        type(cityenter , city);
        sleep(1);
        List<WebElement> citySelection = driver.findElements(By.cssSelector("div[data-post-code]"));
        wait.until(ExpectedConditions.visibilityOfAllElements(citySelection));
        for (int i = 0; i < citySelection.size(); i++) {
            String postalCode = citySelection.get(i).getAttribute("data-post-code");
            if (postalCode.equalsIgnoreCase("21235")) {
                try {
                    List<WebElement> citySelection1 = driver.findElements(By.cssSelector("div[data-post-code]"));
                    citySelection1.get(i).click();
                } catch (Exception e) {
                    List<WebElement> citySelection1 = driver.findElements(By.cssSelector("div[data-post-code]"));
                    citySelection1.get(i).click();
                }

            }
        }
    }

    @Step("Payment selection")
    public void paymentSelection(int option) {
        /*
        1 - Bank Card -- P
        2 - via Account -- P
        3 - Upon delivery -- P
        4 - Bank Card -- C
        5 - via Account -- C
        6 - Upon delivery -- C
        */
        option = option - 1;
        paymentOptions.get(option).click();
    }

    @Step("Delivery selection")
    public void deliveryMethodSelection(int option) {
        /*
        1 - Delivery
        2 - Pick up
        */
        if (option == 1) {
            delivery.click();
        } else if (option == 2) {
            pickUp.click();
        } else {
            System.out.println("You have entered wrong option number");
        }
    }

    @Step("Shop selection")
    public void shopSelection(int option) {
        option = option - 1;
        shopList.get(option).click();
    }

    @Step("Accepting terms")
    public void acceptTerms() {
        sleep(1);
        acceptTerms.click();
    }


    @Step("Submit order")
    public void submitOrder() {
        submit.click();
    }



}