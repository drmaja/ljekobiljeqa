package com.enetelsolutions.ljekobiljeqa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.qameta.allure.Step;

import static com.enetelsolutions.ljekobiljeqa.InitTest.BASE_URL;

public class MainPage extends PageObject {

	public MainPage(WebDriver driver) {
		super(driver);

	}

	Date date = new Date(System.currentTimeMillis());
	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

	Product product;
	JavascriptExecutor js = (JavascriptExecutor) driver;

	// LOCATORS

	// Header area

	@FindBy(css = ".logo.main--list__item  a")
	WebElement headerLogo;

	@FindBy(css = "a.main--list__title")
	List<WebElement> headerLinks;

	/*
	0 - Proizvodi
	1 - O nama
	2 - Kontakt
	3 - Prodavnice
	5 - Pretraga
	6 - Korisnik
	7 - Wishlist
	8 - Korpa
	 */

	@FindBy(id = "fnc-search_field")
	WebElement searchField;

	@FindBy(id = "search_btn")
	WebElement searchBtn;

	@FindBy(css = "span.js-basket-counter")
	WebElement itemsInBasket;



	// Footer area


	// Newsletter area

	@FindBy(css = ".lje_footer_newsletter_action_wrapper input")
	WebElement newsletterEmailInput;

	@FindBy(css = ".lje_footer_newsletter_action_wrapper button")
	WebElement newsletterSubmit;


	// Cookie bar
	@FindBy(css = ".lje_cookie_modal_wrapper_text_wrapper_action_wrapper a")
	WebElement cookieAgree;



	// METHODS

	// Common methods


	@Step("Clearing input fields")
	public void clearField(WebElement element) {
		Actions builder = new Actions(driver);
		Action clearField = builder.click(element).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL)
				.sendKeys(Keys.DELETE).build();
		clearField.perform();
	}

	@Step("Getting element text")
	public String getText(WebElement element) {
		return element.getText();
	}

	@Step("Sending keys to input field")
	public void type(String text, WebElement element) {
		Actions builder = new Actions(driver);
		Action clearField = builder.click(element).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL)
				.sendKeys(Keys.DELETE).build();
		clearField.perform();
		element.sendKeys(text);
	}

	@Step("Moves to element")
	public void moveTo(WebElement element) {

		Actions act = new Actions(driver);
		Action add = act.moveToElement(element).build();
		add.perform();
		sleep(1);
		Action click = act.click().build();
		click.perform();

	}

	@Step("Hovering element")
	public void hover(WebElement element) {
		Actions builder = new Actions(driver);
		Action hoverField = builder.moveToElement(element).build();
		hoverField.perform();
	}

	@Step("Checking if element is displayed")
	public boolean isDisplayed(WebElement element) {
		boolean display = false;
		try {
			display = element.isDisplayed();
		} catch (Exception e) {
			display = false;
		}
		return display;
	}

	@Step("Sleeping")
	public void sleep(double sec) {
		double seconds = sec * 1000;
		long millis = (long) seconds;
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	// Regex and parsing methods
	public double stringToDouble (String s) { // vazi za format cena koje su u basketu npr. "7,483.00 RSD"
		double number = 0;
		try {
			number = Double.parseDouble(s.replaceAll("[A-z\\s\\,]*", ""));
		} catch (Exception e) {
			System.out.println("Greska kod parsiranja cene proizvoda");
		}
		return number;
	}

	@Step("Parse price with decimal places")
	public double decimalToDouble (String s) { // koristi se za cene npr. "1.600 RSD" ili "1.600,34 RSD"
		double number = 0;
		try {
			String removePoint = s.replaceAll("[A-z\\s\\%\\.\\t\\n\\D]*", "");
//			if (removePoint.contains(",")) {
//				removePoint = removePoint.replaceAll(",", ".");
//			}
			number = Double.parseDouble(removePoint);
			number = number / 100;
		} catch (Exception e) {
			System.out.println("Greska kod parsiranja cene proizvoda");
		}

		return number;
	}


	// Header area methods

	@Step("Opening products")
	public void goToProducts() {
		headerLinks.get(0).click();
		sleep(1);
	}

	@Step("Opening About us")
	public void goToAboutUs() {
		headerLinks.get(1).click();
		sleep(1);
	}

	@Step("Opening Contact page")
	public void goToContacts() {
		headerLinks.get(2).click();
		sleep(1);
	}

	@Step("Opening About us")
	public void goToShops() {
		headerLinks.get(3).click();
		sleep(1);
	}

	@Step("Searching for the product")
	public void search(String textsearch) {
		headerLinks.get(5).click();
		sleep(1);
		clearField(searchField);
		searchField.sendKeys(textsearch);
		Actions builder = new Actions(driver);
		Action pressEnter = builder.sendKeys(Keys.ENTER).build(); // search button doesn't exist for this moment
		pressEnter.perform();
	}



	@Step("Going to profile")
	public void goToProfile() {
		headerLinks.get(6).click();
		sleep(1);
	}

	@Step("Going to profile")
	public void goToWishlist() {
		headerLinks.get(7).click();
		sleep(1);
	}

	@Step("Sign-out user")
	public void signOut() {
		goToProfile();
		ProfilePage profile = new ProfilePage(driver);
		profile.logOut.click();
	}


	@Step("Opening basket")
	public void goToCart() {
		driver.get(BASE_URL + "/index.php?mod=cart&op=checkout_page");
//		sleep(4);
//		headerLinks.get(8).click(); vratiti kod nakon spustanja pop-upa ispod header-a
		sleep(1);
	}

	@Step("Counting products in cart")
	public int cartCounter() { //
		sleep(2);
		int counter;
		if (itemsInBasket.isDisplayed()) {
			String cartNumber = itemsInBasket.getText();
			counter = Integer.parseInt(cartNumber);
		} else
			counter = 0;
		return counter;
	}

	@Step("Incrementing +1 actual basket")
	public int cartCountIncrement() {
		int counter = cartCounter();
		counter++;
		return counter;
	}



	// Footer area methods
	
	@Step("Newsletter subscribe")
	public void newsletterSubscribe(String email) {
		type(email, newsletterEmailInput);
		newsletterSubmit.click();
	}


	@Step("Cookie agree")
	public void cookieAgree() {
		try {
			wait.until(ExpectedConditions.visibilityOf(cookieAgree));
			cookieAgree.click();
			sleep(1);
		} catch (Exception e) {

		}
	}

	// Asserting methods

	@Step("Verifying user is signed in")
	public boolean isSignedIn() {
		ProfilePage profile = new ProfilePage(driver);
		wait.until(ExpectedConditions.visibilityOf(profile.logOut));
		return profile.logOut.isDisplayed();
	}

	@Step("Verifying user is not signed in")
	public boolean isNotSignedIn() {
		LoginPage login = new LoginPage(driver);
		wait.until(ExpectedConditions.visibilityOf(login.loginButton));
		return login.loginButton.isDisplayed();
	}

	@Step("Verifying user is not signed in")
	public boolean isAtCartPage() {
		CartPage cp = new CartPage(driver);
		wait.until(ExpectedConditions.visibilityOf(cp.goToCheckout));
		return cp.goToCheckout.isDisplayed();
	}

	// Email generators

	@Step("Generating email adress")
	public String emailAdressGenerator(String type) {
		//String userName = ""+(int)(Math.random()*Integer.MAX_VALUE);
		String userName = "" + date.getTime();
		String emailID ="";
		if (type.equalsIgnoreCase("f")) {
			emailID = userName + "@yopmail.com";
		}
		else if (type.equalsIgnoreCase("p")) {
			emailID = userName + "@mailinator.com";
		}
		else
			System.out.println("Wrong type parameter");

		return emailID;
	}


	public String dateGenerator() {
		String date = dateFormat.format(this.date);
		return date;
	}

	public String timeGenerator() {
		String time = timeFormat.format(this.date);
		return time;
	}



	
	
}
