package com.enetelsolutions.ljekobiljeqa;


import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.github.cdimascio.dotenv.Dotenv;
import io.qameta.allure.Step;

public class RegistrationPage extends MainPage{

	public RegistrationPage(WebDriver driver) {
		super(driver);
	}

	Dotenv dotenv = Dotenv.load();
	public String VALID_PASS = dotenv.get("PASS");


	@FindBy(css = "div[data-value='fizicko']")
	WebElement personalChkBox;

	@FindBy(css = "input#first_name")
	WebElement firstname;

	@FindBy(css = "input#last_name")
	WebElement lastName;

	@FindBy(css = "input[name='customer[email]']")
	WebElement email;

	@FindBy(css = "input[name='customer[mobile]']")
	WebElement phone;

	@FindBy(css = "input[name='customer[password]']")
	WebElement password;

	@FindBy(css = "input[name='customer[confirm_password]']")
	WebElement confirmPassword;

	@FindBy(css = "input[name='customer[street]']")
	WebElement street;

	@FindBy(css="input[name='customer[street_number]']")
	WebElement streetNumber;

	@FindBy(css="input[name='customer[floor]']")
	WebElement floorNumber;

	@FindBy(css="input[name='customer[flat]']")
	WebElement flatNumber;

	@FindBy(css = "input#city")
	WebElement city;

	@FindBy(className = "search-result")
	List<WebElement> cityList;

	@FindBy(css="input#age")
	WebElement datePicker;

	@FindBy(className = "ui-datepicker-month")
	WebElement monthPicker;

	@FindBy(className = "ui-datepicker-year")
	WebElement yearPicker;

	@FindBy(css="#ui-datepicker-div a.ui-state-default")
	List <WebElement> daysPicker;

	@FindBy(css="div[data-name='customer[newsletter]'] .checkbox")
	WebElement newsletterChk;

	@FindBy(css="div[data-name='customer[sms_notifications]'] .checkbox")
	WebElement smsChk;

	@FindBy(css="div[data-name='accept_terms']  .checkbox")
	WebElement acceptTerms;

	@FindBy(css=".lje_register_action button[name='Submit']")
	WebElement registerConfirm;



	// Pravno lice

	@FindBy(css = "div[data-value='pravno']")
	WebElement companyChkBox;

	@FindBy(css = "input[name='customer[company_name]']")
	WebElement companyName;

	@FindBy(css = "input[name='customer[pib]']")
	WebElement vatNumber;


	// Errors

	@FindBy(css = "input#first_name ~ div.form-error")
	WebElement firstnameError;

	@FindBy(css = "input#last_name ~ div.form-error")
	WebElement lastNameError;

	@FindBy(css = "input[name='customer[email]'] ~ div.form-error")
	WebElement emailError;

	@FindBy(css = "input[name='customer[mobile]'] ~ div.form-error")
	WebElement phoneError;

	@FindBy(css = "input[name='customer[password]'] ~ div.form-error")
	WebElement passwordError;

	@FindBy(css = "input[name='customer[confirm_password]'] ~ div.form-error")
	WebElement confirmPasswordError;

	@FindBy(css="input[name='customer[date_of_birth]'] ~ div.form-error")
	WebElement dateError;

	@FindBy(css = "input[name='customer[company_name]'] ~ div.form-error")
	WebElement companyNameError;

	@FindBy(css = "input[name='customer[pib]'] ~ div.form-error")
	WebElement vatNumberError;

	@FindBy(css = "input[name='customer[street]'] ~ div.form-error")
	WebElement streetError;

	@FindBy(css="input[name='customer[street_number]'] ~ div.form-error")
	WebElement streetNumberError;

	@FindBy(css = "input[name='customer[city]'] ~ div.form-error")
	WebElement cityError;


	// Methods

	@Step("user type selection")
	public void selectUserType(int x) {
		if (x == 2) {
			companyChkBox.click();
		}
		else if (x == 1) {
			personalChkBox.click();
		}
	}


	@Step("Fill-out form")
	public void fillForm(Customer customer) {
		type(customer.getFirstName(), firstname);
		type(customer.getLastName(), lastName);
		String emailToUse = customer.getEmail();
		if (emailToUse.contains("@")) {
			customer.setEmail(emailToUse);
		} else if ((customer.getTax().length() == 9)) {
			customer.setEmail(emailAdressGenerator("p"));
		} else {
			customer.setEmail(emailAdressGenerator("f"));
		}
		type(customer.getEmail(), email);
		type(customer.getPassword(), password);
		type(customer.getPassword(), confirmPassword);
		type(customer.getPhone(), phone);
		type(customer.getStreet(), street);
		type(customer.getStreetnumber(), streetNumber);
		type(customer.getFloor(), floorNumber);
		type(customer.getFlat(), flatNumber);
		if ((customer.getTax().length() == 9)) {
			type(customer.getCompanyName(), companyName);
			type(customer.getTax(), vatNumber);
		}

	}


	@Step("City selection")
	public void citySelect(String cityenter) {
		type(cityenter , city);
		sleep(2);
		List<WebElement> citySelection = driver.findElements(By.cssSelector("div[data-post-code]"));
		wait.until(ExpectedConditions.visibilityOfAllElements(citySelection));
		for (int i = 0; i < citySelection.size(); i++) {
			String postalCode = citySelection.get(i).getAttribute("data-post-code");
			if (postalCode.equalsIgnoreCase("21000")) {
				try {
					List<WebElement> citySelection1 = driver.findElements(By.cssSelector("div[data-post-code]"));
					citySelection1.get(i).click();
				} catch (Exception e) {
					List<WebElement> citySelection1 = driver.findElements(By.cssSelector("div[data-post-code]"));
					citySelection1.get(i).click();
				}

			}
		}
	}

	@Step("Date of birth enter")
	public void setDateOfBirth() {
		datePicker.click();
		daysPicker.get(6).click();
	}

	@Step("Confirm registration")
	public void confirm() {
		acceptTerms.click();
		registerConfirm.click();
	}



}




