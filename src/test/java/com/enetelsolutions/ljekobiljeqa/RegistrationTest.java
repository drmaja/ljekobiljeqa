package com.enetelsolutions.ljekobiljeqa;

import io.qameta.allure.Step;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class RegistrationTest extends InitTest {


	@Step("Happy path case Personal")
	@Test(priority = 0, description = "Personal registration happy path")
	public void registrationPersonal() {
		driver.get(BASE_URL + url.loginURL);
		login.goToRegisterForm();
		Customer personal = new Customer(FIRST_NAME_PERSONAL, LAST_NAME_PERSONAL, "pleaseGenerate", PASS_PERSONAL, STREET_PERSONAL,
				STREET_NUMBER_PERSONAL, FLAT_NUMBER_PERSONAL, FLOOR_NUMBER_PERSONAL, PHONE_PERSONAL, "Novi Sad");
		register.fillForm(personal);
		register.setDateOfBirth();
		register.citySelect("Novi S");
		register.confirm();
		assertTrue(register.isSignedIn());
		register.signOut();
	}

	@Step("Happy path case Company")
	@Test(priority = 1, description="Company registration happy path")
	public void registrationCompany() {
		driver.get( BASE_URL + url.loginURL);
		login.goToRegisterForm();
		Customer company = new Customer(FIRST_NAME_COMPANY, LAST_NAME_COMPANY, "pleaseGenerate", PASS_COMPANY, STREET_COMPANY,
				STREET_NUMBER_COMPANY, FLAT_NUMBER_COMPANY, FLOOR_NUMBER_COMPANY, PHONE_COMPANY, "Novi Sad", COMPANY_NAME, COMPANY_TAX);
		register.selectUserType(2);
		register.fillForm(company);
		register.setDateOfBirth();
		register.citySelect("Novi S");
		register.confirm();
		assertTrue(register.isSignedIn());
		register.signOut();

	}




}