package com.enetelsolutions.ljekobiljeqa;

import java.util.concurrent.TimeUnit;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import io.github.cdimascio.dotenv.Dotenv;

public class InitTest {
	static WebDriver driver;

	Dotenv dotenv = Dotenv.load();


	public String VALID_EMAIL = dotenv.get("EMAIL");
	public String VALID_PASS = dotenv.get("PASS");
	public String UNCORRECT_EMAIL = dotenv.get("UNCORRECT_EMAIL");
	public String UNCORRECT_PASS = dotenv.get("UNCORRECT_PASS");
	public String INVALID_NO_MONKEY_EMAIL = dotenv.get("INVALID_NO_MONKEY_EMAIL");
	public String INVALID_NO_DOT_EMAIL = dotenv.get("INVALID_NO_DOT_EMAIL");
	public String INVALID_WITH_SPACE_EMAIL = dotenv.get("INVALID_WITH_SPACE_EMAIL");
	public String INVALID_SHORT_PASS = dotenv.get("INVALID_SHORT_PASS");
	public String INVALID_WITH_SPACE_PASS = dotenv.get("INVALID_WITH_SPACE_PASS");
	public String WL_EMAIL = dotenv.get("WL_EMAIL");
	public String TEMP_EMAIL = dotenv.get("TEMP_EMAIL");
	public String VALID_COMPANY_EMAIL = dotenv.get("COMPANY_EMAIL");


	public String FIRST_NAME = dotenv.get("FIRST_NAME");
	public String LAST_NAME = dotenv.get("LAST_NAME");
	public String EMAIL = dotenv.get("EMAIL2");
	public String PASS = dotenv.get("PASS");
	public String PHONE = dotenv.get("PHONE");
	public String STREET = dotenv.get("STREET");
	public String STREET_NUMBER = dotenv.get("STREET_NUMBER");
	public String FLOOR_NUMBER = dotenv.get("FLOOR_NUMBER");
	public String FLAT_NUMBER = dotenv.get("FLAT_NUMBER");
	public String BANK_CARD = dotenv.get("BANK_CARD");
	public String EXP_MONTH = dotenv.get("EXP_MONTH");
	public String EXP_YEAR = dotenv.get("EXP_YEAR");
	public String BANK_CODE = dotenv.get("BANK_CODE");

	public String FIRST_NAME_PERSONAL = dotenv.get("FIRST_NAME_PERSONAL");
	public String LAST_NAME_PERSONAL = dotenv.get("LAST_NAME_PERSONAL");
	public String PASS_PERSONAL = dotenv.get("PASS_PERSONAL");
	public String PHONE_PERSONAL = dotenv.get("PHONE_PERSONAL");
	public String STREET_PERSONAL = dotenv.get("STREET_PERSONAL");
	public String STREET_NUMBER_PERSONAL = dotenv.get("STREET_NUMBER_PERSONAL");
	public String FLOOR_NUMBER_PERSONAL = dotenv.get("FLOOR_NUMBER_PERSONAL");
	public String FLAT_NUMBER_PERSONAL = dotenv.get("FLAT_NUMBER_PERSONAL");

	public String FIRST_NAME_COMPANY = dotenv.get("FIRST_NAME_COMPANY");
	public String LAST_NAME_COMPANY = dotenv.get("LAST_NAME_COMPANY");
	public String PASS_COMPANY = dotenv.get("PASS_COMPANY");
	public String PHONE_COMPANY = dotenv.get("PHONE_COMPANY");
	public String STREET_COMPANY = dotenv.get("STREET_COMPANY");
	public String STREET_NUMBER_COMPANY = dotenv.get("STREET_NUMBER_COMPANY");
	public String FLOOR_NUMBER_COMPANY = dotenv.get("FLOOR_NUMBER_COMPANY");
	public String FLAT_NUMBER_COMPANY = dotenv.get("FLAT_NUMBER_COMPANY");
	public String COMPANY_NAME = dotenv.get("COMPANY_NAME");
	public String COMPANY_TAX = dotenv.get("COMPANY_TAX");



	public static final String BLANK = "";


	private static String testEnvURL = System.getProperty("environment");
	public static String BASE_URL;


	BankPage bank;
	HomePage home;
	LoginPage login;
	CategoryPage category;
	CartItem ci;
	CartPage cart;
	CheckoutPage checkout;
	ProfilePage profile;
	PromoServicePage pspage;
	RegistrationPage register;
	SingleProductPage spp;
	// SuperCategoryPage supercat;
	SoftAssert sa;
	ThankYouPage ty;
	// JavascriptExecutor js;
	Url url;
	WishlistPage wl;

	@BeforeSuite
	public void setUpSuite() {
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");



		// NORMAL CHROME
//		 driver = new ChromeDriver();

		// HEADLESS MODE

		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--window-size=1920,1080");
		chromeOptions.addArguments("--start-maximized");
		chromeOptions.addArguments("--headless");
		chromeOptions.addArguments("--no-sandbox");
		chromeOptions.addArguments("--disable-gpu");
		chromeOptions.addArguments("--allow-insecure-localhost");
		driver = new ChromeDriver(chromeOptions);


		/*
		 * // FIREFOX *********************
		 * System.setProperty("webdriver.chrome.driver", "/usr/bin/geckodriver"); driver
		 * = new FirefoxDriver();
		 */

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		
	}

	@BeforeSuite
	public String getEnvironment() {

		BASE_URL="";

		if (testEnvURL.equalsIgnoreCase("preprod")) {
			System.out.println("Test scripts are being executed on " + testEnvURL + " environment");
			BASE_URL = "http://ljekobilje-preprod.div.int/";

		} else if (testEnvURL.equalsIgnoreCase("test")){
			System.out.println("Test scripts are being executed on " + testEnvURL + " environment");
			BASE_URL = "http://ljekobilje-test.div.int/";

		} else {
			System.out.println("Please provide ENVIRONMENT PARAMETER");
		}

		return BASE_URL;
				
	}

	

	@BeforeTest
	public void letMeWaitALittle() {
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	}

	

	@BeforeTest
	public void instantiateClass() {
		bank = new BankPage(driver);
		home = new HomePage(driver);
		login = new LoginPage(driver);
		category = new CategoryPage(driver);
		cart = new CartPage(driver);
		ci = new CartItem(driver);
		checkout = new CheckoutPage(driver);
		profile = new ProfilePage(driver);
		pspage = new PromoServicePage(driver);
		register = new RegistrationPage(driver);
		spp = new SingleProductPage(driver);
		// supercat = new SuperCategoryPage(driver);
		sa = new SoftAssert();
		ty = new ThankYouPage(driver);
		// js = (JavascriptExecutor) driver;
		url = new Url(driver);
		wl = new WishlistPage(driver);
	}
	
	

	@AfterMethod
	public void afterMethod(ITestResult testResult) {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			System.out.println(testResult.getName() + ": Failed");
		}
		if (testResult.getStatus() == ITestResult.SUCCESS) {
			System.out.println(testResult.getName() + ": Passed");
		}
	}

	/*@AfterSuite
	public void tearDown() {
		driver.quit();
	}*/


	@Step("Sleeping")
	public void sleep(double sec) {
		double seconds = sec * 1000;
		long millis = (long) seconds;
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}