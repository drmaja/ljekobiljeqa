package com.enetelsolutions.ljekobiljeqa;


public class Customer {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String street;
    private String streetnumber;
    private String flat;
    private String floor;
    private String phone;
    private String city;

    private String companyName;
    private String tax = "0";

    public Customer() {
    
    }

    public Customer(String firstName, String lastName, String email, String pass, String street, String streetnum, String flat, String floor,
                    String phone, String city) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = pass;
        this.street = street;
        this.streetnumber = streetnum;
        this.flat = flat;
        this.floor = floor;
        this.phone = phone;
        this.city = city;
    }

    public Customer(String firstName, String lastName, String email, String pass, String street, String streetnum, String flat, String floor,
                    String phone, String city, String companyName, String tax) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = pass;
        this.street = street;
        this.streetnumber = streetnum;
        this.flat = flat;
        this.floor = floor;
        this.phone = phone;
        this.city = city;
        this.companyName = companyName;
        this.tax = tax;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }


    public String getPassword() {
        return password;
    }

    public String getStreet() {
        return street;
    }

    public String getStreetnumber() {
        return streetnumber;
    }

    public String getFlat() {
        return flat;
    }

    public String getFloor() {
        return floor;
    }

    public String getPhone() {
        return phone;
    }

    public String getCity() {
        return city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getTax() {
        return tax;
    }




    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setStreetnumber(String streetnumber) {
        this.streetnumber = streetnumber;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

}
